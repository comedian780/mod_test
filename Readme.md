# mod_test

Python Modbus-Server for test purposes based on [pymodbus](https://github.com/riptideio/pymodbus/blob/master/examples/common/synchronous_server.py)  

Simply start the container then manipulate the registers with a client application like [QModMaster](https://sourceforge.net/projects/qmodmaster/) to test your client/master device's Modbus TCP connectivity.
-   Registers 0x00 to 0xff get initialized with zeroes
-   Readable with function codes 1 to 4
-   Writable with function codes 5, 6, 15 and 16

## Run the container
```bash
# either run interactive
docker run -i --rm -p 502:502 registry.gitlab.com/comedian780/mod_test

# or run as daemon
docker run -d --rm -p 502:502 --name some_modbus_server registry.gitlab.com/comedian780/mod_test
```

Parameters:
-   **i**: run interactive to view the log output -> check if a connection was established/closed or what data is written/read
-   **d**: run as daemon if the log output is not of interest
-   **rm**: remove the container when stopped
-   **p**: forward the port to the host system -> your computer offers the modbus registers to the network
-   **name**: names the container so it can be easier addressed for logging or stopping


## Build your own container

### Build the container
```bash
# clone the repository to get the code
git clone https://gitlab.com/comedian780/mod_test.git
# enter the directory
cd mod_test
# run the build command
docker build -t mod_test:your_tag .
```

### Start server
```bash
# eiher run interactive
docker run -i --rm -p 502:502 mod_test:your_tag

# or run as daemon
docker run -d --rm -p 502:502 --name some_modbus_server mod_test:your_tag
```
